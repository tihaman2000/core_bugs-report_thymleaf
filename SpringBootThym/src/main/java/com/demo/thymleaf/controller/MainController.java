package com.demo.thymleaf.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.thymleaf.model.Student;
import com.demo.thymleaf.view.StudentForm;

@Controller
public class MainController {
	private static List<Student> students = new ArrayList<Student>();

	static {
		students.add(new Student("Đăng", "Long An", 8));
		students.add(new Student("Hưng", "Đà Nẵng", 10));
		students.add(new Student("Tài", "Vũng Tàu", 9));

	}
	@Value("${welcome.message}")
	private String message;

	@Value("${error.message}")
	private String errorMessage;

	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("message", message);
		return "index";
	}

	@RequestMapping(value = { "/studentList" }, method = RequestMethod.GET)
	public String studentList(Model model) {
		model.addAttribute("students", students);
		return "studentList";
	}

	@RequestMapping(value = { "/addStudent" }, method = RequestMethod.GET)
	public String showAddStudentPage(Model model) {
		StudentForm studentForm = new StudentForm();
		model.addAttribute("studentForm", studentForm);
		return "addStudent";
	}

	@RequestMapping(value = "/addStudent", method = RequestMethod.POST)
	public String saveStudent(Model model, //
			@ModelAttribute("studentForm") StudentForm studentForm) {
		String Name = studentForm.getName();
		String Address = studentForm.getAddress();
		double Mark = studentForm.getMark();
		if (Name != null && Name.length() > 0 && Address != null && Address.length() > 0 && Mark > 0) {
			Student newStudent = new Student(Name, Address, Mark);
			students.add(newStudent);
			return "redirect:/studentList";
		}
		model.addAttribute("errorMessage", errorMessage);
		return "addStudent";

	}
}