package com.demo.thymleaf.model;

public class Student {
	private String Name;
	private String Address;
	private double Mark;
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		this.Name = name;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		this.Address = address;
	}
	public double getMark() {
		return Mark;
	}
	public void setMark(double mark) {
		this.Mark = mark;
	}
	public Student() {
}
	public Student(String Name, String Address, double Mark) {
		this.Name = Name;
		this.Address = Address;
		this.Mark = Mark;
	}
}

