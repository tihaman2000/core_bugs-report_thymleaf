import java.text.DecimalFormat;

public class NhanVienQuanLy extends Staff {
	public NhanVienQuanLy() {
		super();
	}
	DecimalFormat currency = new DecimalFormat("#,###,##0 VND");
	@Override
	public double totalSalary() {
		this.salary = 15000000;
		return this.salary;
	}
	@Override
	public String toString() {
		return super.toString() +"\n, Lương: "+ currency.format(this.salary);
	}
}
