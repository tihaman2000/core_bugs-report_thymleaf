import java.text.DecimalFormat;

public class NhanVienTheoSanPham extends Staff {
	private int totalProduct;
	
	public NhanVienTheoSanPham() {
		super();
	}
	 public NhanVienTheoSanPham(int totalProduct) {
		 super();
		 this.totalProduct = totalProduct;
	 }
	public int getTotalProduct() {
		return totalProduct;
	}
	public void setTotalProduct(int totalProduct) {
		this.totalProduct = totalProduct;
	}
	@Override 
	public void importStaff() {
		super.importStaff();
		System.out.println("Nhập số sản phẩm làm đc: ");
		do {
			totalProduct = scanner.nextInt();
		} while (totalProduct <0); {
			System.out.println("Số sản phẩm lớn hơn 0, nhập lại :");
			totalProduct = scanner.nextInt();
		}
		
	}
	@Override 
	public double totalSalary() {
		this.salary = 10000*this.totalProduct;
		return this.salary;
	}
	DecimalFormat currency = new DecimalFormat("#,###,##0 VND");
	@Override 
	public String toString() {
		return super.toString()+"\n Số sản phẩm làm đc: "+this.totalProduct+", Lương: "+currency.format(this.salary);
	}
}
