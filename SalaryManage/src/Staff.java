import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Staff {
	public String staffId, fullName;
	public boolean gender;
	public int age;
	protected double salary;
	Scanner scanner = new Scanner(System.in);

	static ArrayList<Staff> listStaff = new ArrayList<>();

	public Staff() {
		super();
	}

	public Staff(String staffId, String fullName, boolean sex, int age, double salary) {
		super();
		this.staffId = staffId;
		this.fullName = fullName;
		this.gender = sex;
		this.age = age;
		this.salary = salary;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public boolean getGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public void importStaff() {
		System.out.println("Nhập mã nhân viên: ");
		staffId = scanner.nextLine();
		System.out.println("Nhập họ tên nhân viên: ");

		do {
			fullName = scanner.nextLine();
		} while (fullName.isEmpty() == true);
		try {
			while (checkFullName(fullName) == false) {
				System.out.println("họ tên bằng chữ, vui lòng nhập lại");
				fullName = scanner.nextLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Nhập giới tính: (nhập 'nam' hoặc 'nu') ");
		Scanner scc = new Scanner(System.in);

		String gt = scc.nextLine();
		if (gt.equalsIgnoreCase("nam")) {
			this.gender = true;
		} else if (gt.equalsIgnoreCase("nu")) {
			this.gender = false;
		} else {
			System.out.println("chỉ nhập nam hoặc nu");
			gt = scc.nextLine();
		}

		System.out.println("Nhập tuổi: ");

		age = scanner.nextInt();
		if (age < 18 & age > 65)
		{
			System.out.println("Tuổi chỉ từ 18-65, nhập lại :");
			age = scanner.nextInt();
		}
	}

	public double totalSalary() {
		return 0;
	}

	@Override
	public String toString() {
		return "Mã nhân viên: " + this.staffId + ", Họ và tên: " + this.fullName + ", Giới tính: " + this.gender
				+ ", Tuổi:" + this.age;
	}

	public static boolean checkFullName(String s) throws Exception {
		// kiểm tra họ tên, họ tên chỉ chứa các ký tự từ a -> z và A -> Z
		if (s.matches("[0-9]{1,40}")) {
			return false;
		} else {
			return true;
		}
	}
}
